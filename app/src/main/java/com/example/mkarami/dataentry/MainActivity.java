package com.example.mkarami.dataentry;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView ShowResult;
    EditText FirstName, LastName, PhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("ورود اطلاعات");

        ShowResult = (TextView) findViewById(R.id.ShowResult);
        FirstName = (EditText) findViewById(R.id.FirstName);
        LastName = (EditText) findViewById(R.id.LastName);
        PhoneNumber = (EditText) findViewById(R.id.PhoneNumber);

        findViewById(R.id.ShowInfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowResult.setText("نام : " + FirstName.getText() + System.getProperty("line.separator") + "نام خانوادگی : " + LastName.getText() + System.getProperty("line.separator") + "تلفن : " + PhoneNumber.getText());
            }
        });

    }

}
